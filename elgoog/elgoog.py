import requests
import urllib.parse


def add_page(search_index):
    url = input("URL: ")
    search_depth = int(input("Search depth: "))
    add_page_recursive(search_index, url, search_depth)
    print(search_index)


def strip_between(text, prefix, suffix):
    clean_text = ''
    end_pos = -1
    while True:
        start_pos = text.find(prefix, end_pos+1)
        if start_pos < 0:
            clean_text += " " + text[end_pos:]
            break
        clean_text += " " + text[end_pos+1:start_pos]
        end_pos = text.find(suffix, start_pos)
        if end_pos < 0: break
    return clean_text


def convert_to_search_words(text):
    text = strip_between(text, "<script", "</script>")
    text = strip_between(text, "<style", "</style>")
    text = strip_between(text, "<", ">")

    for char in ".,;:!?'\"":
        text = text.replace(char, ' ')

    return set(text.split(' '))


def add_page_recursive(search_index, url, search_depth):
    print(f"Loading {url}...")
    response = requests.get(url)
    text = response.text.lower()
    
    search_index[url] = convert_to_search_words(text)

    if search_depth > 1:
        links = extract_links(url, text)
        for link in links:
            if link not in search_index:
                add_page_recursive(search_index, link, search_depth-1)


def extract_links(base_url, text):
    links = []
    end_pos = 0
    while True:
        start_pos = text.find("<a ", end_pos)
        if start_pos < 0: break
        end_pos = text.find(">", start_pos)
        if end_pos < 0: break

        tag = text[start_pos+2:end_pos] # ' target="laa" href="sad" style="sdfd"'
        href_pos = tag.find(" href=")
        if href_pos < 0: continue
        start_quote_pos = href_pos + 6 # len(" href=")

        if tag[start_quote_pos] not in ["'", '"']: continue

        quote = tag[start_quote_pos]
        end_quote_pos = tag.find(quote, start_quote_pos+1)
        if end_quote_pos < 0: continue

        link = tag[start_quote_pos+1:end_quote_pos]
        link = urllib.parse.urljoin(base_url, link)
        link = link.split('#')[0]
        links.append(link)

    return links

    # <a target="_blank" href="test">



def search(search_index):
    search = input("Your search query: ")
    search_words = search.lower().split(' ')
    for url, words in search_index.items():
        for search_word in search_words:
            if search_word not in words:
                break
        else:
            print(url)


MAIN_MENU = """
What would you like to do?
1) Add a page
2) Search!
"""

def main():
    search_index = {} # url => list(words)
    while True:
        choice = input(MAIN_MENU)
        if choice=="1":
            add_page(search_index)
        elif choice=="2":
            search(search_index)
        else:
            print("Wrong!")

main()