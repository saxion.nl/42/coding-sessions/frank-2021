from abc import ABC, abstractmethod

class Attribute:
    def __init__(self, name):
        self.name = name

    def to_html(self):
        return self.name


class ValueAttribute(Attribute):
    def __init__(self, name, value):
        super().__init__(name)
        self.value = value

    def to_html(self):
        value = self.value.replace('"', "&quot;")
        return f'{self.name}="{value}"'


class Node(ABC):
    @abstractmethod
    def to_html(self):
        pass


class TextNode(Node):
    def __init__(self, text):
        self.text = text

    def to_html(self):
        return self.text.replace('<', '&lt;').replace('>', '&gt;')


class BaseElement(Node):
    def __init__(self, tag):
        self.tag = tag
        self._attributes = set()

    def add_attribute(self, attr: Attribute):
        self._attributes.add(attr)

    def get_opening_tag(self):
        attr_str = "".join([" "+attr.to_html() for attr in self._attributes])
        return f"<{self.tag}{attr_str}>"


class Element(BaseElement):
    def __init__(self, tag):
        super().__init__(tag)
        self._children = []

    def add_child(self, child: Node):
        self._children.append(child)

    def to_html(self):
        child_html = "".join([child.to_html() for child in self._children])
        return f"{self.get_opening_tag()}{child_html}</{self.tag}>"


class SelfClosingElement(BaseElement):
    def to_html(self):
        return self.get_opening_tag()


class Document(Element):
    def __init__(self, doctype = "html"):
        super().__init__("html")
        self.doctype = doctype

    def to_html(self):
        return f"<!DOCTYPE {self.doctype}>\n" + super().to_html() 


doc = Document()
doc.add_attribute(ValueAttribute("lang", "en"))

head = Element("head")

title = Element("title")
title.add_child(TextNode("This is great!"))
head.add_child(title)

link = SelfClosingElement("link")
link.add_attribute(ValueAttribute("rel", "stylesheet"))
link.add_attribute(ValueAttribute("href", "style.css"))
head.add_child(link)

body = Element("body")
for _ in range(100):
    body.add_child(TextNode("Hello world!"*10))

doc.add_child(head)
doc.add_child(body)


print(doc.to_html())