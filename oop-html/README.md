# HTML generator

It's going to be awesome!



## Class diagram

```plantuml

class Document {
    +doctype: str
    +to_html(): str
}

Document --|> Element

abstract Node {
    {abstract}+to_html(): str
}

class Attribute {
    +name: str
    +to_html(): str
}

class ValueAttribute extends Attribute {
    +value: str
    +to_html(): str
}

class TextNode extends Node {
    +text: str
    +to_html(): str
}


abstract class BaseElement extends Node {
    +tag: str
}

class SelfClosingElement extends BaseElement {
    +to_html(): str
}

class Element extends BaseElement {
    +to_html(): str
}

Element "children" --> "0..*" Node

BaseElement --> "0..*" Attribute

```