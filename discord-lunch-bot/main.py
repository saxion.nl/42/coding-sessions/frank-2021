from discord_components import DiscordComponents, ComponentsBot, Button
from discord import NotFound
import asyncio
from models import Lunch, LunchList, Attendee
import datetime
import sqlalchemy
import sqlalchemy.orm

TOKEN = "OTE3Nzk2MjA3NTUwNjY0NzY1.Ya96Uw.lsOnJEQY21Vbu7G9BZverKM-5bY"

db_engine = sqlalchemy.create_engine("sqlite:///database.sqlite")
Session = sqlalchemy.orm.sessionmaker(db_engine)
bot = ComponentsBot(command_prefix = "!")


async def sleep_until(hour, minute):
    now = datetime.datetime.now()

    # Set the next trigger time to the next 8:30, assuming hour==8 and minute==30
    # (which is either today, if it's really early now, or tomorrow).
    next_trigger_time = now.replace(hour = hour, minute = minute, second = 0, microsecond = 0)
    if next_trigger_time < now:
        next_trigger_time += datetime.timedelta(days = 1)

    seconds = (next_trigger_time - now).total_seconds()
    print("sleep seconds", seconds)
    seconds = 10
    await asyncio.sleep(seconds)



async def perform_time_based_action():
    while True:
        await sleep_until(8, 30)
        await send_lunch_request()

        # TODO: reminders!

        await sleep_until(10, 00)
        await send_fetcher_request()


async def send_lunch_request():
    with Session() as session:
        for lunch_list in session.query(LunchList).all():
            channel = await bot.fetch_channel(lunch_list.channel_key)
            if not channel:
                print(f"Unable to find channel {lunch_list.channel_key}")
                continue
            
            await channel.send(
                "Would you like to join lunch today?",
                components = [
                    Button(label = "Yes please!", custom_id = "yes"),
                    Button(label = "No thanks.", custom_id = "no")
                ],
            )


QUERY = """
select
    a.user_key,
    coalesce(
        -l.cost / (
            select count(*)
            from attendees a2
            where a2.lunch_id = l.id
        )
    , 0) + coalesce(
        case l.lunch_fetcher_key = a.user_key
        when true then +l.cost
        else 0
        end
    ,0 ) balance
from lunches l
join attendees a on a.lunch_id = l.id
where l.lunch_list_id=:lunch_list_id and l.cost is not null
group by a.user_key
order by balance
"""


async def send_balances(session, lunch_list_id, channel):
    text = "Here are the current balances:\n"
    rows = session.execute(sqlalchemy.sql.text(QUERY).params(lunch_list_id = lunch_list_id)).all()
    for row in rows:
        try:
            user = await bot.fetch_user(row.user_key)
            name = user.display_name
        except NotFound:
            name = row.user_key
        text += f"{name}: {row.balance}\n"

    await channel.send(text)

async def send_fetcher_request():
    with Session() as session:
        for lunch_list in session.query(LunchList).all():
            channel = await bot.fetch_channel(lunch_list.channel_key)
            if not channel:
                print(f"Unable to find channel {lunch_list.channel_key}")
                continue

            await send_balances(session, lunch_list.id, channel)

            # TODO:
            # - bovenstaande query uitvoeren
            # - user names opzoeken obv ids
            # - vragen wie de lunch gaat halen met een button
            # - (en dat alles hoeft niet als er minder dan 2 personen hebben ingeschreven)
            
            await channel.send(
                "Who will do the shopping?",
                components = [
                    Button(label = "Me!", custom_id = "become_shopper"),
                ],
            )


@bot.event
async def on_ready():
    print(f"Logged in as {bot.user}!")
    asyncio.create_task(perform_time_based_action())


@bot.command()
async def start_lunch_bot(ctx):
    try:
        with Session() as session:
            lunch_list = LunchList()
            lunch_list.channel_key = ctx.channel.id
            lunch_list.guild_key = ctx.guild.id
            session.add(lunch_list)
            session.commit()
        await ctx.send("LunchBot created.")
    except sqlalchemy.exc.IntegrityError:
        await ctx.send("LunchBot already installed in channel.")


@bot.command()
async def balances(ctx):
    with Session() as session:
        lunch_list = session.query(LunchList).filter_by(channel_key = ctx.channel.id).one()
        await send_balances(session, lunch_list.id, ctx.channel)


@bot.command()
async def paid(ctx, amount):
    with Session() as session:
        lunch = get_lunch(ctx.channel.id, session)
        if lunch.lunch_fetcher_key != str(ctx.author.id):
            await ctx.send("You are not the lunch fetcher, you fool!")
            return

        try:
            print(ctx.args, ctx.args[0].args, amount)
            lunch.cost = float(amount)
        except ValueError:
            await ctx.send("That is not a number.")
            return
        session.commit()

        await send_balances(session, lunch.lunch_list_id, ctx.channel)


def get_lunch(channel_key, session):
    lunch_list = session.query(LunchList).filter_by(channel_key = channel_key).one()
    today = datetime.date.today()
    try:
        lunch = session.query(Lunch).filter_by(lunch_list_id = lunch_list.id, date=today).one()
    except sqlalchemy.exc.NoResultFound:
        lunch = Lunch()
        lunch.lunch_list_id = lunch_list.id
        lunch.date = today
        session.add(lunch)
        session.commit()
    return lunch


@bot.event
async def on_button_click(interaction):
    print(interaction.user, interaction.channel.id)
    # if interaction.responded:
    #     return
    
    with Session() as session:
        lunch = get_lunch(interaction.channel.id, session)

        if interaction.custom_id == "yes":
            attendee = Attendee()
            attendee.lunch_id = lunch.id
            attendee.user_key = interaction.user.id
            try:
                session.add(attendee)
                session.commit()
                await interaction.send(content = "Have a great lunch!")
            except sqlalchemy.exc.IntegrityError:
                await interaction.send(content = "You were already attending.")

        elif interaction.custom_id == "no":
            session.query(Attendee).filter_by(lunch_id = lunch.id, user_key = interaction.user.id).delete()
            session.commit()
            await interaction.send(content = "Boooh!")

        elif interaction.custom_id == "become_shopper":
            if lunch.lunch_fetcher_key:
                if lunch.lunch_fetcher_key == str(interaction.user.id):
                    lunch.lunch_fetcher_key = None
                    await interaction.channel.send(f"{interaction.user.display_name} chickened out! Who will now do the shopping?")
                else:
                    await interaction.send(content = "Somebody else is already doing the shopping.")
            else:
                lunch.lunch_fetcher_key = interaction.user.id
                await interaction.channel.send(f"{interaction.user.display_name} will do the shopping!")
                await interaction.send("Let the bot know the amount of money you spent using something like: !paid 42.42")
            session.commit()

        else:
            print(f"Invalid interaction: {interaction.custom_id}")

bot.run(TOKEN)
