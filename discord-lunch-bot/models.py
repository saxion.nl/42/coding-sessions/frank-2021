import sqlalchemy
import sqlalchemy.orm


Base = sqlalchemy.orm.declarative_base()


class LunchList(Base):
    __tablename__ = 'lunch_lists'

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    channel_key = sqlalchemy.Column(sqlalchemy.String, unique=True, nullable=False)
    guild_key = sqlalchemy.Column(sqlalchemy.String, nullable=False)


class Lunch(Base):
    __tablename__ = 'lunches'

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    lunch_list_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('lunch_lists.id'), nullable=False)
    date = sqlalchemy.Column(sqlalchemy.Date, nullable=False)

    lunch_list = sqlalchemy.orm.relationship("LunchList", backref="lunches")

    lunch_fetcher_key = sqlalchemy.Column(sqlalchemy.String)
    cost = sqlalchemy.Column(sqlalchemy.Float)

    __table_args__ = (
        sqlalchemy.UniqueConstraint('lunch_list_id', 'date', name='_lunch_date_uc'),
    )


class Attendee(Base):
    __tablename__ = 'attendees'

    user_key = sqlalchemy.Column(sqlalchemy.String, nullable=False, primary_key=True)
    lunch_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('lunches.id'), nullable=False, primary_key=True)

    lunch = sqlalchemy.orm.relationship("Lunch", backref="attendees")
