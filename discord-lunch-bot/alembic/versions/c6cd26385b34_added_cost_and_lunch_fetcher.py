"""added cost and lunch fetcher

Revision ID: c6cd26385b34
Revises: ce7b1a2c725f
Create Date: 2022-01-25 17:01:30.628226

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c6cd26385b34'
down_revision = 'ce7b1a2c725f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('lunches', sa.Column('lunch_fetcher_key', sa.String(), nullable=True))
    op.add_column('lunches', sa.Column('cost', sa.Float(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('lunches', 'cost')
    op.drop_column('lunches', 'lunch_fetcher_key')
    # ### end Alembic commands ###
