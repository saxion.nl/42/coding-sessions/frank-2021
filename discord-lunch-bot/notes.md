## Discord tokens

- application id: 917796207550664765
- public key: 8bd8957f8f665fc0117c532217a2c8775bd9f5b3699f07f6ffc3b84ff1648a09
- bot token: OTE3Nzk2MjA3NTUwNjY0NzY1.Ya96Uw.lsOnJEQY21Vbu7G9BZverKM-5bY


## Functional design

	Elke ochtend om 8:30: wie luncht er mee? ja/nee

	Elke ochtend om 10:00: random meeluncher kiezen en hem/haar vragen of er nog genoeg lunch is. ja/nee

	!balances: laat zien wie hoeveel betaald heeft en gebruikt heeft

	Als er lunch gehaald moet worden worden de balances getoond, en een knop 'Ik haal die f#cking boodschappen wel'.

	!paid 25.21

	Idee:
	- Voor meerdere dagen lunch halen
	- Wanneer er lunch wordt gehaald, wordt het bedrag van de vorige shopping session gedeeld over alle lunches die nog geen bedrag hebben.


## Technical stack

- sqlite
- sqlalchemy
- migrations (alembic)


## Alembic commands

- alembic revision --autogenerate -m "initial database schema"
- alembic upgrade head


## Docs

- `discord_component` documentation: https://devkiki7000.gitbook.io/discord-components/guide/getting-started
- `discord_component` gebruikt `discordpy`: https://discordpy.readthedocs.io/en/stable/ext/commands/api.html

