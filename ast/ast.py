from abc import ABC, abstractmethod


class Component(ABC):
    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def display(self, indent=""):
        pass


class Literal(Component):
    def __init__(self):
        self._data = input("Please type the value:")

    def run(self):
        return self._data

    def display(self, indent=""):
        print(f"{indent}Literal\n{indent}  data: {self._data}")



class Nothing(Component):
    def run(self):
        pass

    def display(self, indent=""):
        print(f"{indent}Nothing")


class If(Component):
    def __init__(self):
        self._condition = create_component("Select condition-type:")
        self._then = create_component("Select then-type:")
        self._else = create_component("Select else-type:")

    def run(self):
        if self._condition.run():
            return self._then.run()
        else:
            return self._else.run()

    def display(self, indent=""):
        print(f"{indent}If\n{indent}  condition:")
        self._condition.display(indent+"    ")
        print(f"{indent}  then:")
        self._then.display(indent+"    ")
        print(f"{indent}  else:")
        self._else.display(indent+"    ")


COMPONENTS = [Literal, If, Nothing]

def create_component(prompt=""):
    while True:
        for index, SpecificComponent in enumerate(COMPONENTS):
            print(f"{index+1}. {SpecificComponent.__name__}")

        try:
            SpecificComponent = COMPONENTS[int(input(prompt)) - 1]
        except (IndexError, ValueError):
            print("WRONG, you idiot!")
        else:
            return SpecificComponent()
            

print("\nWelcome to AST!")
component = create_component()
component.display()
result = component.run()
print(f"Result: {result}")
