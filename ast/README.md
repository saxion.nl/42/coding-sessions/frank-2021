## This is what the program should look like

```
Welcome to AST!

If
  condition:
    Comparison
      left:
        FunctionCall
          arguments: []
      right:
        Literal
          data: "hallo"
  then:
    FunctionCall
      arguments:
        - Literal
          data: "hallo terug"
```

For the equivalent Python file:

```python
if input() == "hallo":
    print("hallo terug")

```

## Class diagram

```plantuml

abstract class Component {
    +run() {abstract}
    +display(indent) {abstract}
}

class If {
    +run()
    +display(indent)
}

If --|> Component
If -> Component : condition
If -> Component : then

FunctionCall --|> Component
FunctionCall --> "0..*" Component : arguments

class Literal {
    -data: str
}
Literal --|> Component

Comparison --|> Component
Comparison --> Component : left
Comparison --> Component : right
class Comparison {
    -operator: str
}
```