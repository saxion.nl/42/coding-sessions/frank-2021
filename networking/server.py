import asyncio

async def retrieve_url(url, client_writer):
    if url[:7] != 'http://':
        client_writer.write(bytes("Not supported!\n", "utf-8"))
        return

    url_parts = url[7:].split('/')
    host = url_parts[0]
    path = "/" + "/".join(url_parts[1:])

    http_reader, http_writer = await asyncio.open_connection(host, 80)
    http_writer.write(bytes(f"GET {path} HTTP/1.0\nHost: {host}\n\n", "utf-8"))
    response = await http_reader.read(-1)
    http_writer.close()

    client_writer.write(bytes(f"The length for {url} is: {len(response)}\n", "utf-8"))


async def handle_client(reader, writer):
    addr = writer.get_extra_info('peername')
    print(f"Connection from {addr!r}")
    while True:
        writer.write(bytes("Type a URL you'd like to know the body size of: ", "utf-8"))

        url = (await reader.readline()).strip()

        asyncio.create_task(retrieve_url(str(url, "utf-8"), writer))

    # data = await reader.read(100)
    # message = data.decode()
    # addr = writer.get_extra_info('peername')

    # print(f"Received {message!r} from {addr!r}")

    # print(f"Send: {message!r}")
    # writer.write(data)
    # await writer.drain()

    # print("Close the connection")
    # writer.close()
    # await writer.wait_closed()


async def main():
    server = await asyncio.start_server(handle_client, '0.0.0.0', 8888)

    # async with server:
    await server.serve_forever()

asyncio.run(main())
