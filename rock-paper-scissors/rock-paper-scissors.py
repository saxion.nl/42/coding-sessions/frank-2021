import random

print("""
------------------------------
Welcome to Rock Paper Scissors
------------------------------""")

while True:
    random_choice = random.random()

    if random_choice < 1/3:
        bot_hand = 'rock'
    elif random_choice < 2/3:
        bot_hand = 'paper'
    else:
        bot_hand = 'scissors'

    while True:
        player_hand = input("Rock, paper of scissors? ").lower()
        if player_hand == "rock" or player_hand == "paper" or player_hand == "scissors":
            break

    print("Bot hand:", bot_hand)
    print("Player hand:", player_hand)

    if bot_hand != player_hand:
        break
    
    print("It's a tie!")


if \
  (player_hand == 'rock' and bot_hand == 'paper') or \
  (player_hand == 'paper' and bot_hand == 'scissors') or \
  (player_hand == 'scissors' and bot_hand == 'rock'):
    print('You lose!')
else:
    print('You win!')
