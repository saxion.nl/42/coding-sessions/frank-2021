# The stack pointer, it always hold the next address where something
# can be pushed onto it. We initialize it to the bottom of the stack.
sp: int16 stackBottom

# Variables
boxCount: int32 0
fromStack: int16 0
toStack: int16 0
stackHeightPtr: int16 0
charPtr: int16 0
char: int8 0

# General purpose 'registers'
a: int32 0
b: int32 0

# Memory for the supply stacks is dynamically allocated in main
supplyStackPtr: int16 0

# The number of boxes stored on each of the 9 supply stacks.
supplyStackHeights:
	int16 0
	int16 0
	int16 0
	int16 0
	int16 0
	int16 0
	int16 0
	int16 0
	int16 0



macro call FUNCTION {
	set16 **sp *ip # Copy the address of this instruction into **sp
	add16 **sp 24 # skip the 24 bytes for all instruction in this macro
	add16 *sp 2 # Increment the stack pointer
	set16 *ip FUNCTION
}

return:
	sub16 *sp 2
	set16 *ip **sp



# ---- readInt32 ----
# Returns:
# - b: The number read from the stdin. Left-strips any non-number characters.
# Destroys:
# - a

readInt32:
	# left-strip
	read32 *b
	if<32 *b '0' set16 *ip readInt32
	if>32 *b '9' set16 *ip readInt32
	sub32 *b '0'

	# b contains the first digit
readInt32_nextDigit:
	read32 *a
	if<32 *a '0' set16 *ip return
	if>32 *a '9' set16 *ip return
	sub32 *a '0'
	mul32 *b 10
	add32 *b *a
	set16 *ip readInt32_nextDigit



# ---- pushToSupplyStack ----
# Arguments:
# - toStack: The supply stack index
# - char: The character to append
# Destroys:
# - stackHeightPtr
# - charPtr

pushToSupplyStack:
	# Calculate the address for the appropriate stack height into stackHeightPtr
	set16 *stackHeightPtr supplyStackHeights
	add16 *stackHeightPtr *toStack
	add16 *stackHeightPtr *toStack

	# Calculate the address for the target character into charPtr
	set16 *charPtr *toStack
	mul16 *charPtr 100
	add16 *charPtr *supplyStackPtr
	add16 *charPtr **stackHeightPtr

	# Set the actual character
	set8 **charPtr *char

	# Increment the supply stack height for this stack
	add16 **stackHeightPtr 1
	
	set16 *ip return



# ---- popFromSupplyStack ----
# Arguments:
# - fromStack: The supply stack index
# Returns:
# - char: The popped character or '!' if the stack was empty
# Destroys:
# - stackHeightPtr
# - charPtr

popFromSupplyStack:
	# Load the from-stack-height address into stackHeightPtr
	set16 *stackHeightPtr supplyStackHeights
	add16 *stackHeightPtr *fromStack
	add16 *stackHeightPtr *fromStack

	if=16 **stackHeightPtr 0 set16 *ip popFromSupplyStack_empty

	# Decrement the from-stack-height by 1
	sub16 **stackHeightPtr 1

	# Calculate the address of the character we're moving into charPtr
	set16 *charPtr *fromStack
	mul16 *charPtr 100
	add16 *charPtr *supplyStackPtr
	add16 *charPtr **stackHeightPtr

	# Set the character we're moving to char
	set16 *char **charPtr

	set16 *ip return

popFromSupplyStack_empty:
	set8 *char '!'
	set16 *ip return



# ---- main ----

main:
	# Reserve 9*100 bytes for the supply_stacks on the stack
	set16 *supplyStackPtr *sp
	add16 *sp 900

startNextLine:
	# Read an integer into boxCount
	call readInt32
	set32 *boxCount *b

	# Jump to handlers for our two special cases for boxCount
	if=32 *boxCount 0 set16 *ip addNewBox
	if=32 *boxCount 255 set16 *ip showResults

	# Read an integer into fromStack
	call readInt32
	set16 *fromStack *b
	sub16 *fromStack 1

	# Read an integer into toStack
	call readInt32
	set16 *toStack *b
	sub16 *toStack 1

moveNextBox:
	# Stop repeating this section after *boxCount times
	if=16 *boxCount 0 set16 *ip startNextLine
	sub16 *boxCount 1

	# Pop from stack `fromStack` into `char`
	call popFromSupplyStack

	# Push char onto stack toStack
	call pushToSupplyStack

	# Repeat *boxCount times
	set16 *ip moveNextBox

addNewBox:
	# Read an integer into toStack
	call readInt32
	sub32 *b 1
	set16 *toStack *b

	# Read the character to store into char
	read8 *char

	# Push char onto stack toStack
	call pushToSupplyStack

	# Read the next line
	set16 *ip startNextLine

showResults:
	set16 *fromStack 0
	
showResultsLoop:
	if=16 *fromStack 9 set16 *ip endProgram
	
	call popFromSupplyStack

	if=8 *char '!' set16 *ip skipChar
	write8 *char

skipChar:
	add16 *fromStack 1
	set16 *ip showResultsLoop

endProgram:
	write8 '\n'
	set16 *ip 0


# The stack starts here. It has quite a bit of room to grow: the size of 
# the architecture's memory (65536 bytes) minus the size of the program.
stackBottom:
