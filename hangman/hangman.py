import random

def create_word_list():
    with open('/usr/share/dict/american-english', 'r') as file:
        words = []
        for line in file:
            word = line.strip()
            for char in word:
                if char < 'a' or char > 'z':
                    break
            else:
                if len(word) >= 4 and len(word) < 10 and 'xx' not in word:
                    words.append(word)
    return words


WORDS = create_word_list()

GALLOWS = """
  3333333333
  22       4
  22      555
  22       5
  22   99 666 AA
  22      666
  22     7   8
  22     7   8
1111111111111111111
"""


def mask_word(word, guessed_letters):
    masked_word = ''
    for char in word:
        if char in guessed_letters:
            masked_word += char
        else:
            masked_word += '_'
    return masked_word


def draw_gallows(wrong_guesses):
    gallows = GALLOWS
    for guess_number in range(wrong_guesses, 10):
        char = str(guess_number+1)
        if char == "10":
            char = "A"
        gallows = gallows.replace(char, " ")
    print(gallows)


def play_game():

    print("\nWelcome to hangman!")

    word = random.choice(WORDS)
    guessed_letters = ''
    wrong_guesses = 0

    while wrong_guesses < 10:
        draw_gallows(wrong_guesses)

        masked_word = mask_word(word, guessed_letters)
        print(masked_word)
        if "_" not in masked_word:
            print("You won!")
            return

        guess = input("Your guess: ")
        if len(guess)!=1 or guess < 'a' or guess > 'z':
            print("That's not a valid letter")
        elif guess in guessed_letters:
            print("You already said that...")
        elif guess in word:
            print("Jeej!")
        else:
            print('Nope!')
            wrong_guesses += 1

        guessed_letters += guess


    draw_gallows(wrong_guesses)
    print("You lost!")
    print(f"The word was: {word}")


while True:
    play_game()

