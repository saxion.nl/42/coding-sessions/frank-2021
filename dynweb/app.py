from datetime import datetime
import flask
from flask_sqlalchemy import SQLAlchemy
import postgresqlite
import flask_wtf
import wtforms as wtf
from wtforms.validators import DataRequired, IPAddress


app = flask.Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = postgresqlite.get_uri()
app.secret_key = "A#BM)C)qx@exqWETYBTZSWERDdfTFGHJNP<_"
db = SQLAlchemy(app) 


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)
    city = db.Column(db.String(), nullable=False)
    # description = db.Column(db.String(), nullable=False)
    # info = db.Column(db.String(), nullable=False)
    # max_students = db.Column(db.Integer(), nullable=False)

class CompanyForm(flask_wtf.FlaskForm):
    name = wtf.StringField('Name', validators=[DataRequired()])
    city = wtf.StringField('City', validators=[DataRequired()])
    # description = wtf.StringField('Description', validators=[DataRequired()])
    # info = wtf.StringField('Info', validators=[DataRequired()])
    # max_students = wtf.IntegerField('Maximum student count', validators=[DataRequired()])
    submit = wtf.SubmitField('Save')

class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)


class Registration(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    
    company_id = db.Column(db.Integer(), db.ForeignKey('company.id'), nullable=False)
    company = db.relationship('Company', backref='registrations')

    student_id = db.Column(db.Integer(), db.ForeignKey('student.id'), nullable=False)

    safari_day_id = db.Column(db.Integer(), db.ForeignKey('safari_day.id'), nullable=False)


safari_day_company_table = db.Table(
    "safari_day_company",
    db.Column("company_id", db.ForeignKey("company.id")),
    db.Column("safari_day_id", db.ForeignKey("safari_day.id")),
)

class SafariDay(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date(), nullable=False)
    available_companies = db.relationship("Company", backref="safari_days", secondary=safari_day_company_table)



with app.app_context():
    db.create_all()


# class LoginForm:
#     user_name
#     submit_button
#     pass

# LoginLink?

@app.route('/', methods=['GET', 'POST'])
def main_page():
    # sd = SafariDay.query.get(1)
    # for company in sd.available_companies:
    #     print(company.max_students)

    company_form = CompanyForm()

    if company_form.validate_on_submit():
        company = Company()
        company_form.populate_obj(company)
        db.session.add(company)
        db.session.commit()

        company_form = CompanyForm()

    return flask.render_template("home.html", company_form=company_form)
