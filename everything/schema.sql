drop schema public cascade;
create schema public;

create table "user" (
    id serial primary key,
    name text not null unique
);

create table post (
    id serial primary key,
    message text not null,
    user_id int not null references "user"(id)
);

insert into "user"(name) values('Frank');
insert into "user"(name) values('Rens');
insert into "user"(name) values('Tim');

insert into "post"(message, user_id)
select 'Hello Frank', "user".id
from "user"
where name='Rens';

insert into "post"(message, user_id)
select 'Hello Rens & Frank', "user".id
from "user"
where name='Tim';

insert into "post"(message, user_id)
select 'Blablablah', "user".id
from "user"
where name='Tim';
