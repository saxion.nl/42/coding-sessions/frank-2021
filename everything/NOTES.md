```plantuml

entity User {
    *id: int
    ---
    *name: text
}

entity Post {
    *id: int
    ---
    *message: text
    *user_id: int <<FK>>
}

User ||--o{ Post

```