import postgresqlite
from flask import Flask, render_template, request, redirect
import random

db = postgresqlite.connect()

app = Flask("app")
app.config['TEMPLATES_AUTO_RELOAD'] = True

POST_QUERY = '''
select message, name
from post
join "user" on "user".id = post.user_id
order by post.id desc
'''

@app.route("/", methods=["GET"])
def index():
    posts = db.query(POST_QUERY)
    return render_template("index.html", posts=posts)


@app.route("/", methods=["POST"])
def add_yeet():
    name = request.form["name"]
    message = request.form["message"]

    user = db.query_row('select * from "user" where name=:name', name=name)
    if user==None:
        user = db.query_row('insert into "user"(name) values(:name) returning *', name=name)

    db.query('insert into post(user_id,message) values(:user_id,:message)', user_id=user.id, message=message)

    return redirect("/")


@app.route("/test")
def test():
    return f"<h1>This is a test</h1>"
