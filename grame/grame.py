import blessed
from abc import ABC, abstractmethod

# TODO:
# - implement select_square (with fancy cursor selection)
# - create a pygame-based version (that uses the mouse for selection)

class Entity:
    pass

class GridGame(ABC):
    def __init__(self, width, height, square_width=3):
        self._width = width
        self._height = height
        self._square_width = square_width
        self._squares = {}
        self._term = blessed.Terminal()

    def play(self):
        """The main game function that will call `run_game` after creating
        the output window/terminal settings.
        """
        with self._term.fullscreen(), self._term.cbreak():
            self.run_game()

    def get_entity(self, x, y):
        return self._squares.get((x,y), None)

    def set_entity(self, x, y, entity: Entity):
        self._squares[(x,y)] = entity

    def draw(self):
        with self._term.fullscreen(), self._term.cbreak():
            header = ("x" + ("-" * self._square_width)) * self._width + "x"
            print(header)
            for y in range(self._height):
                row = "x"
                for x in range(self._width):
                    entity = self.get_entity(x,y)
                    # We'll draw the str value of the entity, or "" if there is None.
                    field = str(entity)[:self._square_width] if entity else ""

                    # Add padding to field to make it self._square_width long,
                    # alternating between left and right, to center the content.
                    for pad in range(self._square_width - len(field)):
                        if pad % 2:
                            field = " " + field
                        else:
                            field += " "

                    row += field + "x"

                print(row)
                print(header)
            input()

    @abstractmethod
    def run_game(self):
        """Games should implement this method to run the full game. Once
        this method returns, the game is finished."""
        pass
