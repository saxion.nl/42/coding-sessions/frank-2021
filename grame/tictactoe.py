import grame

class TicTacToe(grame.GridGame):
    def __init__(self):
        super().__init__(3, 3)

    def run_game(self):
        players = [Player('X'), Player('O')]
        self.set_entity(2, 1, players[0])
        self.set_entity(2, 2, players[1])
        self.set_entity(0, 2, players[0])
        self.draw()

class Player(grame.Entity):
    def __init__(self, marker):
        self._marker = marker

    def __str__(self):
        return self._marker
    
if __name__ == "__main__":    
    TicTacToe().play()
