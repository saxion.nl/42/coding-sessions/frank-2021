## Class diagram
```plantuml
package lib {

    class GridGame {
        -width: int
        -height: int
        -squares: dict[tuple, Entity]
        +draw()
        +get_entity(x,y): Entity
        +select_square(): Entity
    }

    class Entity {

    }

    GridGame o--> "0..*" Entity
}

package TicTacToe {
    class TicTacToeGame {

    }

    GridGame <|-- TicTacToeGame

    class Player {
        -marker: str
        -grid_game: GridGame

        play_turn()
    }

    Entity <|-- Player

    TicTacToeGame o--> "2" Player


}

```