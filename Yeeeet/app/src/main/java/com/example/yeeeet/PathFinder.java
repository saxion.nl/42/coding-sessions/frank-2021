package com.example.yeeeet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

public class PathFinder {

  static class NextTileOption {
    Coordinate position;
    int ticksSpent;
    Coordinate previousPosition;

    public NextTileOption(Coordinate position, int ticksSpent, Coordinate previousPosition) {
      this.position = position;
      this.ticksSpent = ticksSpent;
      this.previousPosition = previousPosition;
    }
  }


  static ArrayList<Coordinate> findPath(Map map, Coordinate position, Coordinate target) {

    PriorityQueue<NextTileOption> nextTileOptions = new PriorityQueue(100, new Comparator<NextTileOption>() {
      @Override
      public int compare(NextTileOption o1, NextTileOption o2) {
        return o1.ticksSpent - o2.ticksSpent;
      }
    });
    nextTileOptions.add(new NextTileOption(position, 0, null));

    HashMap<Coordinate,Coordinate> previousTiles = new HashMap<>();

    Coordinate closestTile = position;

    while(!nextTileOptions.isEmpty()) {
      // Explore the tile that currently has the smallest cost to visit
      NextTileOption option = nextTileOptions.poll();

      // If we already reached this tile using a faster path, we don't need to explore it again
      if (previousTiles.containsKey(option.position)) {
        continue;
      }

//      System.out.println("option position="+option.position+" previousPosition"+option.previousPosition);

      // Store the previous tile that leads to the current position
      previousTiles.put(option.position, option.previousPosition);

      // Did we reach the target?
      if (option.position.equals(target)) {
//        System.out.println("We found a route!");
        closestTile = target;
        break;
      }

      // Set closestTile to the current position, if the current position is closer to the target
      // than anything we've seen yet
      if (option.position.getDistance(target) < closestTile.getDistance(target)) {
        closestTile = option.position;
      }

      // Add the adjacent squares to the options
      for(Coordinate nextPosition : option.position.getNeighbours()) {
        int cost = map.getTravelCost(nextPosition) * 100;
        if (cost >= 0 && !previousTiles.containsKey(nextPosition)) {
          nextTileOptions.add(new NextTileOption(nextPosition, option.ticksSpent + cost, option.position));
        }
      }

      // Add the diagonal squares to the options
      for(Coordinate nextPosition : option.position.getDiagonalNeighbours()) {
        // When driving diagonally, we effectively driving over four squares, so let's check the
        // other two can be traversed as well.
        if (map.getTravelCost(new Coordinate(option.position.x, nextPosition.y)) < 0) {
          continue;
        }
        if (map.getTravelCost(new Coordinate(nextPosition.x, option.position.y)) < 0) {
          continue;
        }

        int cost = map.getTravelCost(nextPosition) * 141; // 100 * sqrt(2)
        if (cost >= 0 && !previousTiles.containsKey(nextPosition)) {
          nextTileOptions.add(new NextTileOption(nextPosition, option.ticksSpent + cost, option.position));
        }
      }
    }

//    System.out.println("Unit closestTitle "+closestTile);

    // Calculate the cheapest path from position to closestTile
    ArrayList<Coordinate> path = new ArrayList<>();
    while(closestTile != position && closestTile != null) {
      path.add(closestTile);
      closestTile = previousTiles.get(closestTile);
    }

    Collections.reverse(path);

    return path;
  }
}
