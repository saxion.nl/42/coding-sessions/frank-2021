package com.example.yeeeet;

import java.util.Objects;

class Coordinate {
  int x;
  int y;

  public Coordinate(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public Coordinate[] getNeighbours() {
    return new Coordinate[] {
      new Coordinate(this.x + 1, this.y + 0),
      new Coordinate(this.x - 1, this.y + 0),
      new Coordinate(this.x + 0, this.y + 1),
      new Coordinate(this.x + 0, this.y - 1)
    };
  }

  public Coordinate[] getDiagonalNeighbours() {
    return new Coordinate[] {
      new Coordinate(this.x + 1, this.y + 1),
      new Coordinate(this.x - 1, this.y - 1),
      new Coordinate(this.x + 1, this.y - 1),
      new Coordinate(this.x - 1, this.y + 1)
    };
  }
  public int getDistance(Coordinate other) {
    return Math.abs(x - other.x) + Math.abs(y - other.y);
  }

  @Override
  public String toString() {
    return "Coordinate{" +
      "x=" + x +
      ", y=" + y +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Coordinate that = (Coordinate) o;
    return x == that.x && y == that.y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }
}
