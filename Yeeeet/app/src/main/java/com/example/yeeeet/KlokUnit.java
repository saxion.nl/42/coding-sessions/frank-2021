package com.example.yeeeet;

import android.content.Context;
import android.graphics.Canvas;

public class KlokUnit extends Unit {

  long timeLeft = 10000;

  public KlokUnit(Map map, Coordinate coordinate) {
    super(map, coordinate);
  }

  @Override
  public void draw(Canvas canvas, Context context, long timeDelta) {
    super.draw(canvas, context, timeDelta);

    timeLeft -= timeDelta;
    if (timeLeft <= 0) {
      map.removeUnit(this);
    }
  }

  @Override
  int getMsPerTile() {
    return 1000;
  }

  @Override
  int getDrawableResourceId() {
    return R.drawable.klok;
  }

  @Override
  public void calculateNewPosition() {
    if (position.equals(map.player.position)) {
      map.player.drinkKlok();
      map.removeUnit(this);
    }
  }
}
