package com.example.yeeeet;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

public class MapView extends View {

  private Map map;
  final int TILE_SIZE = 10; // dp (device pixels)

  double scalingFactor, minScalingFactor, maxScalingFactor;
  double translateX = 0;
  double translateY = 0;
  long previousTime;


  public MapView(Context context, Map map) {
    super(context);
    this.map = map;

    float dpr = getResources().getDisplayMetrics().density;
    scalingFactor = dpr * TILE_SIZE;
    minScalingFactor = scalingFactor;
    maxScalingFactor = scalingFactor * 6;
    previousTime = System.currentTimeMillis();
  }


  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    canvas.save();

    translateX = Math.max(translateX, -map.MAP_WIDTH*scalingFactor + canvas.getWidth());
    translateY = Math.max(translateY, -map.MAP_HEIGHT*scalingFactor + canvas.getHeight());

    translateX = Math.min(translateX, 0);
    translateY = Math.min(translateY, 0);

    canvas.translate((float)translateX, (float)translateY);
    canvas.scale((float)scalingFactor, (float)scalingFactor);

    long currentTime = System.currentTimeMillis();

    map.draw(canvas, this.getContext(), currentTime-previousTime);

    canvas.restore();
    map.drawOnScreenDisplay(canvas, this.getContext(), currentTime-previousTime);

    previousTime = currentTime;

    invalidate();
  }


  double lastDistance = -1;
  double lastX = -1, lastY = -1;
  double tapStartX = -1, tapStartY = -1;

  @Override
  public boolean onTouchEvent(MotionEvent event) {

    System.out.println(event.getAction()+ " / " + lastX + "," + lastY);
    if (event.getPointerCount() > 0 && event.getAction() != MotionEvent.ACTION_UP && event.getActionMasked() != MotionEvent.ACTION_POINTER_UP) {
      double avgX = 0, avgY = 0;
      for(int pointerIndex=0; pointerIndex < event.getPointerCount(); pointerIndex++) {
        avgX += event.getX(pointerIndex);
        avgY += event.getY(pointerIndex);
      }
      avgX /= (double)event.getPointerCount();
      avgY /= (double)event.getPointerCount();
      System.out.println(avgX + "," + avgY);

      if (lastX >= 0 && event.getAction() == MotionEvent.ACTION_MOVE) {
        translateX += avgX - lastX;
        translateY += avgY - lastY;
      }
      lastX = avgX;
      lastY = avgY;

      if (event.getPointerCount() == 2) {
        double distance = Math.sqrt(
          Math.pow(event.getX(0) - event.getX(1), 2) +
            Math.pow(event.getY(0) - event.getY(1), 2)
        );

        if (lastDistance > 0) {
          double prevScalingFactor = scalingFactor;
          scalingFactor = Math.max(minScalingFactor, Math.min(maxScalingFactor, scalingFactor * distance / lastDistance));
          translateX -= (avgX-translateX) * (scalingFactor / prevScalingFactor - 1) ;
          translateY -= (avgY-translateY) * (scalingFactor / prevScalingFactor - 1) ;
        }
        lastDistance = distance;
      }
      else {
        lastDistance = -1;
      }

    } else {
      lastX = lastY = lastDistance = -1;
    }



    if (event.getPointerCount() == 1) {
      // See if we're trapping
      float x = event.getX();
      float y = event.getY();
      if (event.getAction() == MotionEvent.ACTION_DOWN) {
        // Possibly starting a new tap
        tapStartX = x;
        tapStartY = y;
      }
      else if (tapStartX >= 0){
        // We might still be in a tap
        if (Math.pow(x- tapStartX,2) + Math.pow(y- tapStartY,2) > 25) {
          // Moved finger more than 25px. This is not a tap.
          tapStartX = tapStartY = -1;
        }
        else if (event.getAction() == MotionEvent.ACTION_UP) {
          // Up again, while at no point moving more than 25px. This is an actual tap.
          map.handleTap((int) ((x - translateX) / scalingFactor), (int) ((y - translateY) / scalingFactor));
        }
      }
    } else {
      // We require exactly one finger for a tap.
      tapStartX = tapStartY = -1;
    }

    return true;
  }
}
