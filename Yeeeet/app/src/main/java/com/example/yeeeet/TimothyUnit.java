package com.example.yeeeet;

import java.util.Random;

public class TimothyUnit extends Unit {
  TimothyUnit(Map map, Coordinate coordinate) {
    super(map, coordinate);
  }

  @Override
  int getMsPerTile() {
    return 200;
  }

  @Override
  int getDrawableResourceId() {
    return R.drawable.timot;
  }

  @Override
  protected void onTargetReached() {
    Random random = new Random();
    setTarget(random.nextInt(map.MAP_WIDTH), random.nextInt(map.MAP_HEIGHT));
  }
}
