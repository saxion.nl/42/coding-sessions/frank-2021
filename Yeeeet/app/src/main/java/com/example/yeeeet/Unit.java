package com.example.yeeeet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

abstract public class Unit {
  Coordinate position;
  Coordinate target;

  long smoothTimeHad = 0; // The number of milliseconds we have already transitioned between two adjacent squares
  long smoothTimeTotal = 0; // The total number of milliseconds needed to make the transition
  Coordinate smoothStartPosition; // The original position from which we're doing a smooth transition

  Bitmap image;
  Map map;

  Unit(Map map, Coordinate coordinate) {
    this.map = map;
    position = coordinate;
    smoothStartPosition = position;
    target = position;
  }

  abstract int getMsPerTile();

  abstract int getDrawableResourceId();

  public void draw(Canvas canvas, Context context, long timeDelta) {

    if (image == null) {
      image = BitmapFactory.decodeResource(context.getResources(), getDrawableResourceId());
    }

    // Move between smoothStartPosition and position. When we've reached position, try to set a
    // new position based on target.
    smoothTimeHad += timeDelta;
    while (smoothTimeHad >= smoothTimeTotal) {
      smoothStartPosition = position;
      calculateNewPosition();
      smoothTimeHad -= smoothTimeTotal;
      smoothTimeTotal = map.getTravelCost(position) * getMsPerTile();
      if (smoothTimeTotal < 0) {
        // The player cannot move because he/she is on top of water/mountain
        break;
      }
      if (smoothStartPosition.x != position.x && smoothStartPosition.y != position.y) {
        // Moving diagnally: slow down according to Pythagoras
        smoothTimeTotal = (int)((double)smoothTimeTotal * 1.41);
      }
    }

    // Interpolate between smoothStartPosition and position
    float fraction = (float)smoothTimeHad / (float)smoothTimeTotal;
    float x = (float)smoothStartPosition.x + fraction * (position.x - smoothStartPosition.x);
    float y = (float)smoothStartPosition.y + fraction * (position.y - smoothStartPosition.y);

    RectF targetRect = new RectF(x, y, x + 1, y + 1);
    canvas.drawBitmap(image, null, targetRect, null);
  }

  public void setTarget(int x, int y) {
    target = new Coordinate(x,y);
  }

  protected void onTargetReached() {}

  public void calculateNewPosition() {
    if (target.equals(position)) {
      onTargetReached();
      if (target.equals(position)) {
        return;
      }
    }

    ArrayList<Coordinate> path = PathFinder.findPath(map, position, target);
//    System.out.println("Unit path "+path);

    if (!path.isEmpty()) {
      target = path.get(path.size()-1);
      position = path.get(0);
    }
  }
}
