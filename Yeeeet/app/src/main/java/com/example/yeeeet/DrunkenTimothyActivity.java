package com.example.yeeeet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DrunkenTimothyActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_drunken_timothy);

    SharedPreferences sharedPreferences = getSharedPreferences("highscore", MODE_APPEND);
    int highscore = sharedPreferences.getInt("highscore", 0);

    Intent intent = getIntent();
    if (intent != null) {
      int score = intent.getIntExtra("score", 0);

      TextView scoreText = findViewById(R.id.scoreText);
      if (score<0) {
        scoreText.setText("Geen kereltjes voor jou!");
      } else {
        if (score > highscore) {
          SharedPreferences.Editor editor = sharedPreferences.edit();
          editor.putInt("highscore", score);
          editor.commit();
        }
        scoreText.setText(score + " / " + highscore);
      }
    }

    Button newGameButton = findViewById(R.id.newGameButton);
    newGameButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(DrunkenTimothyActivity.this, MainActivity.class));
      }
    });
  }
}