package com.example.yeeeet;

import android.content.Context;
import android.graphics.Canvas;

public class PlayerUnit extends Unit {
  int klokLeftTime = 0; // Power-up is enabled when > 0

  PlayerUnit(Map map, Coordinate coordinate) {
    super(map, coordinate);
  }

  @Override
  int getMsPerTile() {
    return klokLeftTime > 0 ? 125 : 250;
  }

  @Override
  int getDrawableResourceId() {
    return R.drawable.penguin_car;
  }

  @Override
  public void draw(Canvas canvas, Context context, long timeDelta) {
    super.draw(canvas, context, timeDelta);

    klokLeftTime -= timeDelta;
    if (klokLeftTime < 0) klokLeftTime = 0;
  }

  public void drinkKlok() {
    klokLeftTime += 10000;
  }

}
