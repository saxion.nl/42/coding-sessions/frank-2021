package com.example.yeeeet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/*
 * TODO:
 * - A-star! (testen met grotere maps)
 * - Deel van de map buiten beeld niet tekenen
 * - Power up: standaard lopen met pinquin, veranderen in auto voor 20s.
 * - Path van Unit niet bij elk vakje opnieuw berekenen maar gewoon bijhouden in een lijstje
 */

public class Map {
  final int MAP_WIDTH = 90;
  final int MAP_HEIGHT = 50;

  Random random = new Random();
  HashMap<TileType, Bitmap> tileMap; // intentionally left null
  TileType[][] map = new TileType[MAP_WIDTH][MAP_HEIGHT];

  int score = 1000000;

  TimothyUnit timothy;
  FrankUnit frank;
  PlayerUnit player;

  Paint scorePaint;


  enum TileType {
    LAND,
    WATER,
    FOREST,
    ROCKS
  }

  ArrayList<Unit> units = new ArrayList<>();

  Coordinate getRandomCoordinate() {
    return new Coordinate(random.nextInt(MAP_WIDTH), random.nextInt(MAP_HEIGHT));
  }

  public Map() {
    generate();
//      if (getTravelCost(new Coordinate(x,y)) > 0) {

    ArrayList<Coordinate> path;
    do {
      path = PathFinder.findPath(this, getRandomCoordinate(), getRandomCoordinate());
    } while(path.size() < 20);

    player = new PlayerUnit(this, path.get(0));
    units.add(player);

    timothy = new TimothyUnit(this, path.get(path.size()-1));
    units.add(timothy);

    frank = new FrankUnit(this, path.get(path.size()-1));
    units.add(frank);

    scorePaint = new Paint();
    scorePaint.setColor(Color.BLACK);
    scorePaint.setTextSize(42);
    scorePaint.setShadowLayer(6.0f, 0.0f, 0.0f, Color.WHITE);
    scorePaint.setAntiAlias(true);
    scorePaint.setTextAlign(Paint.Align.CENTER);
  }


  public boolean hasCoordinate(Coordinate coord) {
    return coord.x >=0 && coord.y >= 0 && coord.x < MAP_WIDTH && coord.y < MAP_HEIGHT;
  }


  private void generate() {
    for (int x = 0; x < MAP_WIDTH; x++) {
      for (int y = 0; y < MAP_HEIGHT; y++) {
        map[x][y] = TileType.WATER;
      }
    }
    randomFill(TileType.LAND, 1, 0.4 + random.nextDouble()*0.4, TileType.LAND);
    randomFill(TileType.ROCKS, 5, random.nextDouble()*0.15, TileType.WATER);
    randomFill(TileType.FOREST, 50, random.nextDouble()*0.15, TileType.WATER);
  }

  private void randomFill(TileType tileType, int startingPointCount, double fillRate, TileType avoidTileType) {

    // Generate random starting points
    ArrayList<Coordinate> possibleSquares = new ArrayList<Coordinate>();
    for(int i=0; i<startingPointCount; i++) {
      possibleSquares.add(new Coordinate(random.nextInt(MAP_WIDTH), random.nextInt(MAP_HEIGHT)));
    }

    int squaresLeft = (int)((double)MAP_WIDTH * (double)MAP_HEIGHT * fillRate);

    while(squaresLeft > 0) {
      // Select a random coordinate to fill with land
      int coordinatePosition = random.nextInt(possibleSquares.size());
      Coordinate coordinate = possibleSquares.get(coordinatePosition);

      // Remove the selected coordinate from the list of possible coordinates
      Coordinate lastCoordinate = possibleSquares.remove(possibleSquares.size()-1);
      if (coordinatePosition < possibleSquares.size()) {
        possibleSquares.set(coordinatePosition, lastCoordinate);
      }

      // Skip if the tile is already land
      if (map[coordinate.x][coordinate.y] == tileType || map[coordinate.x][coordinate.y] == avoidTileType) continue;

      // Make the tile land
      map[coordinate.x][coordinate.y] = tileType;

      // Add all non-land neighbours to the list of possible squares
      for(Coordinate neighbour : coordinate.getNeighbours()) {
        if (neighbour.x < 0 || neighbour.x >= MAP_WIDTH) continue;
        if (neighbour.y < 0 || neighbour.y >= MAP_HEIGHT) continue;
        if (map[neighbour.x][neighbour.y] == tileType) continue;
        possibleSquares.add(neighbour);
      }

      // We've made some progress!
      squaresLeft--;
    }

  }


  private void loadBitmaps(Context context) {
    if (tileMap != null) return;
    tileMap = new HashMap<>();

    tileMap.put(TileType.WATER, BitmapFactory.decodeResource(context.getResources(), R.drawable.water));
    tileMap.put(TileType.LAND, BitmapFactory.decodeResource(context.getResources(), R.drawable.grass));
    tileMap.put(TileType.ROCKS, BitmapFactory.decodeResource(context.getResources(), R.drawable.mountain));
    tileMap.put(TileType.FOREST, BitmapFactory.decodeResource(context.getResources(), R.drawable.forest));
  }

  public int getTravelCost(Coordinate pos) {
    if (!hasCoordinate(pos)) return -1; // pos is outside the map

    TileType tileType = map[pos.x][pos.y];
    if (tileType==TileType.LAND) return 1;
    if (tileType==TileType.FOREST) return 2;
    return -1;
  }

  public void draw(Canvas canvas, Context context, long timeDelta) {
    loadBitmaps(context);

//    System.out.println("draw");

    for(int x=0; x<MAP_WIDTH; x++) {
      for(int y=0; y<MAP_HEIGHT; y++) {
        RectF targetRect = new RectF(
          (float)x,
          (float)y,
          (float)(x+1),
          (float)(y+1)
        );
        canvas.drawBitmap(tileMap.get(map[x][y]), null, targetRect, null);
      }
    }

    for(Unit unit : units) {
      unit.draw(canvas, context, timeDelta);
    }

    if (player.position.equals(timothy.position) || player.position.equals(timothy.smoothStartPosition)) {
      units = new ArrayList<>(); // end the game
      Intent intent = new Intent(context, DrunkenTimothyActivity.class);
      intent.putExtra("score", score);
      context.startActivity(intent);
    }

    if (player.position.equals(frank.position) || player.position.equals(frank.smoothStartPosition)) {
      units = new ArrayList<>(); // end the game
      Intent intent = new Intent(context, DrunkenTimothyActivity.class);
      intent.putExtra("score", -1);
      context.startActivity(intent);
    }

    if (random.nextDouble() < (double)timeDelta/1000.0) {
      Coordinate position = new Coordinate(random.nextInt(MAP_WIDTH), random.nextInt(MAP_HEIGHT));
      units.add(new KlokUnit(this, position));
    }
  }

  public void drawOnScreenDisplay(Canvas canvas, Context context, long timeDelta) {
    score -= timeDelta;

    canvas.drawText(""+score, canvas.getWidth()/2, 69, scorePaint);
  }


  void handleTap(int x, int y) {
    units.get(0).setTarget(x,y);
  }

  public void removeUnit(Unit unit) {
    units = (ArrayList)units.clone();
    units.remove(unit);
  }

}
