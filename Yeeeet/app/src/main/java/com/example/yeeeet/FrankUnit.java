package com.example.yeeeet;

import java.util.ArrayList;
import java.util.Random;

public class FrankUnit extends Unit {

  public FrankUnit(Map map, Coordinate coordinate) {
    super(map, coordinate);
  }

  @Override
  int getMsPerTile() {
    return 300;
  }

  @Override
  int getDrawableResourceId() {
    return R.drawable.frank;
  }

  @Override
  public void calculateNewPosition() {
    ArrayList<Coordinate> path = PathFinder.findPath(map, position, map.player.position);

    if (!path.isEmpty()) {
      position = path.get(0);
    }
  }


}
