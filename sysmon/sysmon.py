from abc import abstractmethod, ABC
import time, os, sys, inspect


class SysMon:
    def __init__(self):
        self.__stats = []
        while True:
            print("Please select a statistic type:")

            options = []

            for name, MyClass in inspect.getmembers(sys.modules[__name__]):
                if inspect.isclass(MyClass) and issubclass(MyClass, Stat) and MyClass != Stat:
                    options.append(MyClass)

                    print(f"{len(options)}. {MyClass.DESCRIPTION}")

            print("0. Done!")

            try:
                selection = input(">> ").strip()
                if selection == "0":
                    break
                MyClass = options[int(selection)-1]
            except (TypeError, IndexError):
                print("Invalid input!")
                continue

            self.__stats.append(MyClass())
            


    def run(self):
        while True:
            print()
            for stat in self.__stats:
                print(f"{stat.get_name()+':':30} {stat.get_value()}")

            time.sleep(1)


class Stat(ABC):
    @abstractmethod
    def get_name():
        pass

    @abstractmethod
    def get_value():
        pass


class ProcMemoryStat(Stat):
    DESCRIPTION = "Memory used by a specific process"

    def __init__(self):
        self.__pid = input("PID: ")

    def get_name(self):
        return f"Memory of #{self.__pid}"
        
    def get_value(self):
        try:
            with open(os.path.join('/proc', self.__pid, 'status')) as file:
                for line in file:
                    if line.startswith('VmRSS:'):
                        return line.split()[1]+"kb"
        except FileNotFoundError:
            return "No such process"


class TemperateStat(Stat):
    DESCRIPTION = "Temperature for a specific sensor"



if __name__ == '__main__':
    SysMon().run()
