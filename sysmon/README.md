## Example

CPU load               73%
Size of /tmp/x.txt     323456
Memory of #213         1234kb
Memory of #456         333kb
Temp sensor 1          23 C


## Class diagram

```plantuml

class SysMon {

}

abstract class Stat {
    {abstract} +get_name()
    {abstract} +get_value()
}

SysMon --> "0..*" Stat

class ProcMemoryStat {
}

class TemperatureStat {
}

Stat <|-- ProcMemoryStat
Stat <|-- TemperatureStat

```