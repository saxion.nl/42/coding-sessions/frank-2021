import random, math
from gamelib import SpriteEntity, LabelEntity, Game


class BackgroundImage(SpriteEntity):
    def __init__(self, game):
        super().__init__(game, "stars.png", -1)
        self.x = game.width / 2
        self.y = game.height / 2
        self.scale = 0.01

    def update(self, dt):
        self.scale += 0.1 * dt
        self.opacity -= 0.1 * dt
        self.rotation += self.scale * 50 * dt

        if self.opacity <= 0:
            self.remove()


class Bullet(SpriteEntity):
    BULLET_SPEED = 600
    def __init__(self, game, rotation, x, y):
        super().__init__(game, "pipe-top.png")
        self.scale = 0.05

        radians = (rotation + 90) / 360 * 2 * math.pi
        self.x = x + 50 * math.sin(radians)
        self.y = y + 50 * math.cos(radians)
        self.__x_speed = self.BULLET_SPEED * math.sin(radians)
        self.__y_speed = self.BULLET_SPEED * math.cos(radians)

    def update(self, dt):
        self.x += self.__x_speed * dt
        self.y += self.__y_speed * dt
        self.rotation += 360 * dt


class SpaceBirds(SpriteEntity):
    def __init__(self, game, player_number):
        super().__init__(game, "bird.png")

        self.__x_speed = 0 # pixels/second
        self.__y_speed = 0 # pixels/second
        self.__cooldown_time = 0 # in seconds

        self.__player_number = player_number
        if player_number == 0:
            self.x = 100
            self.y = 100
            self.rotation = 180
        else:
            assert(player_number == 1)
            self.x = game.width - 100
            self.y = game.height - 100

    def shoot(self):
        Bullet(self.game, self.rotation, self.x, self.y)

    def update(self, dt):
        self.__cooldown_time -= dt

        print(self.game.keys_down)
        if "LEFT" in self.game.keys_down:
            self.rotation -= 180*dt
        elif "RIGHT" in self.game.keys_down:
            self.rotation += 180*dt
        elif "UP" in self.game.keys_down:            
            thrust = 400 * dt
            radians = (self.rotation + 90) / 360 * 2 * math.pi
            self.__x_speed += thrust * math.sin(radians)
            self.__y_speed += thrust * math.cos(radians)
        elif "DOWN" in self.game.keys_down:
            if self.__cooldown_time <= 0:
                self.shoot()
                self.__cooldown_time = 0.5

        if self.x > self.game.width/2:
            self.__x_speed -= 50*dt
        else:
            self.__x_speed += 50*dt
        if self.y > self.game.height/2:
            self.__y_speed -= 50*dt
        else:
            self.__y_speed += 50*dt

        self.x += self.__x_speed * dt
        self.y += self.__y_speed * dt


class BirdWars(Game):
    def __init__(self):
        super().__init__(800, 800)
        self.__next_background_time = 0

    def start(self):
        SpaceBirds(self, 0)
        SpaceBirds(self, 1)

    def update(self, dt):
        self.__next_background_time -= dt
        if self.__next_background_time <= 0:
            BackgroundImage(self)
            self.__next_background_time = 1

BirdWars().run()

