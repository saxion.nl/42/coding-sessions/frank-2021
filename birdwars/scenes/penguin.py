from scenelib import Scene

class Penguin(Scene):
    SPEED = 300 # px / second

    def __init__(self, distance):
        self.__distance = distance

    def create(self):
        self.__penguin = self.add_entity("bird.png")

    def get_duration(self):
        return self.__distance / self.SPEED # seconds

    def get_width(self):
        return self.__distance + self.__penguin.width # pixels

    def animate(self, delta_time, time):
        self.__penguin.left = self.x_offset + time * self.SPEED
