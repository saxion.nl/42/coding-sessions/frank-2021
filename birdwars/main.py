from scenelib import Contraption
from scenes.penguin import Penguin

Contraption(
    Penguin(200),
    Penguin(400),
    Penguin(200),
).run()
