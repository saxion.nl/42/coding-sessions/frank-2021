from gamelib import SpriteEntity, Game
from abc import ABC, abstractmethod

FLOOR_Y = 100

class Scene(ABC):
    # contraption # will be set by contraption
    # x_offset # will be set by contraption
    # y_offset # will be set by contraption
    # start_time # will be set by contraption

    @abstractmethod
    def create(self):
        pass

    @abstractmethod
    def get_width(self):
        pass

    @abstractmethod
    def get_duration(self):
        pass

    @abstractmethod
    def animate(self, delta_time: float, time: float):
        pass

    def add_entity(self, image_file, layer=0, **kwargs):
        return SceneEntity(self,image_file, layer=0, **kwargs)

    def _animate(self, delta_time, time):
        relative_time = time - self.start_time
        if relative_time >= 0:
            duration = self.get_duration()
            if relative_time-delta_time < duration:
                self.animate(min(delta_time, relative_time), min(relative_time,duration))


class SceneEntity(SpriteEntity):
    def __init__(self, scene, image_file, layer=0, **kwargs):
        super().__init__(scene.contraption, "images/"+image_file, layer, **kwargs)
        self.scene = scene
        self.left = scene.x_offset
        self.bottom = scene.y_offset


class Contraption(Game):
    def __init__(self, *scenes):
        super().__init__(960, 540)

        x_offset = 100
        start_time = 1
        for scene in scenes:
            scene.contraption = self
            scene.x_offset = x_offset
            scene.y_offset = FLOOR_Y
            scene.start_time = start_time

            scene.create()

            x_offset += scene.get_width()
            start_time += scene.get_duration()

        self.__scenes = scenes

    def update(self, delta_time, total_time):
        # TODO: determine which scene to update (binary search! :-))
        for scene in self.__scenes:
            scene._animate(delta_time, total_time)
