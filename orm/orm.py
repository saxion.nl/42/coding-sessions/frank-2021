import sqlite3

db = sqlite3.connect("database.sqlite3")

objects_to_write = set()


class FilterQuery:
    def __init__(self, a, b, operator):
        self._a = a
        self._b = b
        self._operator = operator

    def get_sql(self, cls):
        field_name = cls.get_field_name(self._a)
        return f"{field_name} {self._operator} ?", [self._b],

    def __and__(self, other):
        return CombinedFilterQuery(self, other, "and")

    def __or__(self, other):
        return CombinedFilterQuery(self, other, "or")


class CombinedFilterQuery(FilterQuery):
    def get_sql(self, cls):
        sql_a, args_a = self._a.get_sql(cls)
        sql_b, args_b = self._b.get_sql(cls)
        return f"({sql_a}) {self._operator} ({sql_b})", args_a+args_b,


class Table:

    def __init__(self, **kwargs):
        self._status = "INSERT"
        objects_to_write.add(self)

        # Initialize default values
        for name, field in self.__class__.__dict__.items():
            if isinstance(field, Field):
                setattr(self, name, field.default)

        # Set the values provided by the constructor arguments
        for name, value in kwargs.items():
            if not getattr(self.__class__, name, None):
                raise ValueError(f"No such database field '{name}'")
            setattr(self, name, value)

    @classmethod
    def get_field_name(cls, field):
        for name, value in cls.__dict__.items():
            if value is field:
                return name
        raise ValueError("Field is not type of table")

    @classmethod
    def create(cls):
        field_sqls = []
        pk_names = []
        for name, field in cls.__dict__.items():
            if isinstance(field, Field):
                field_sql = f"{name} {field.datatype}"
                if not field.allow_null:
                    field_sql += " NOT NULL"
                if field.primary_key:
                    pk_names.append(name)
                field_sqls.append(field_sql)

        if pk_names:
            field_sqls.append(f"PRIMARY KEY({', '.join(pk_names)})")
        fields_sql = ', '.join(field_sqls)
        sql = f"CREATE TABLE IF NOT EXISTS {cls._get_name()}({fields_sql})"
        print(sql)
        db.execute(sql)

    @classmethod
    def get_pk_field_names(cls):
        pks = []
        for name, field in cls.__dict__.items():
            if isinstance(field, Field) and field.primary_key:
                pks.append(name)
        return pks 

    @classmethod
    def _get_name(cls):
        return cls.__name__.lower()

    @classmethod
    def get(cls, *values):
        field_names = cls.get_pk_field_names()
        if len(values) != len(field_names):
            raise ValueError(f"Invalid number of primary key values given")

        field_sqls = [f"{name} = ?" for name in field_names]
        fields_sql = ' AND '.join(field_sqls)

        return cls.select_one(f"WHERE {fields_sql}", *values)

    @classmethod
    def select_one(cls, *args):
        results = cls.select(*args)
        if len(results) == 1:
            return results[0]
        elif len(results) == 0:
            return None
        else:
            raise ValueError("Too many rows")

    @classmethod
    def select_first(cls, *args):
        results = cls.select(*args)
        if len(results) == 0:
            return None
        else:
            return results[0]

    @classmethod
    def select(cls, sql="", *args, order_by=()):
        if isinstance(sql, FilterQuery):
            condition, args = sql.get_sql(cls)
            sql = "WHERE " + condition

        sql = f"SELECT * FROM {cls._get_name()} " + sql
        print(sql, args)

        if order_by:
            if not isinstance(order_by, list) and not isinstance(order_by, tuple):
                order_by = (order_by,)
            sql += " ORDER BY " + ",".join([item.get_order_sql(cls) for item in order_by])

        results = []
        cursor = db.execute(sql, args)
        rows = cursor.fetchall()
        for row in rows:
            obj = cls()
            for value, description in zip(row, cursor.description):
                name = description[0]
                setattr(obj, name, value)
            obj._status = None
            objects_to_write.remove(obj)
            results.append(obj)

        return results

    def _write(self):
        if self._status == "INSERT":
            names = []
            values = []
            for name, value in self.__dict__.items():
                if not name.startswith('_'):
                    names.append(name)
                    values.append(value)

            question_marks = ','.join(['?']*len(values))
            names = ','.join(names)
            sql = f"INSERT INTO {self._get_name()}({names}) VALUES({question_marks})"

            print(sql, values)
            db.execute(sql, values)

        elif self._status == "UPDATE":
            values = []

            set_sql = []
            for name, field in self.__class__.__dict__.items():
                if isinstance(field, Field) and not field.primary_key:
                    set_sql.append(f"{name}=?")
                    values.append(getattr(self,name))
                    
            where_sql = []
            for name in self.__class__.get_pk_field_names():
                where_sql.append(f"{name}=?")
                values.append(getattr(self,name))

            sql = f"UPDATE {self._get_name()} SET {','.join(set_sql)} WHERE {' and '.join(where_sql)}"
            print(sql, values)
            db.execute(sql, values)

        self._status = None

    def __setattr__(self, name, value):
        if not name.startswith('_') and not self._status:
            self._status = "UPDATE"
            objects_to_write.add(self)

        super().__setattr__(name, value)

    def __str__(self):
        pk_values = [str(getattr(self,name)) for name in self.get_pk_field_names()]
        return self.__class__.__name__ + ":" + ":".join(pk_values)

    def __repr__(self):
        result = []
        for name, value in self.__dict__.items():
            if not name.startswith('_'):
                result.append(f"{name}={value}")
        return self.__class__.__name__ + ' { ' + (' '.join(result)) + ' }'



class Field:
    def __init__(self, datatype, primary_key=False, foreign_key=None, allow_null=False, default=None):
        self.datatype = datatype
        self.primary_key = primary_key
        self.foreign_key = foreign_key
        self.allow_null = allow_null
        self.default = default

    def __eq__(self, other):
        return FilterQuery(self, other, "=")

    def __ne__(self, other):
        return FilterQuery(self, other, "<>")

    def __lt__(self, other):
        return FilterQuery(self, other, "<")

    def __gt__(self, other):
        return FilterQuery(self, other, ">")

    def __le__(self, other):
        return FilterQuery(self, other, "<=")

    def __ge__(self, other):
        return FilterQuery(self, other, ">=")

    def __mod__(self, other):
        return FilterQuery(self, other, "LIKE")

    def asc(self):
        return OrderDirection(self, "asc")

    def desc(self):
        return OrderDirection(self, "desc")
                
    def get_order_sql(self, cls):
        return cls.get_field_name(self) + " asc"


class OrderDirection:
    def __init__(self, field, direction):
        self.field = field
        self.direction = direction
    
    def get_order_sql(self, cls):
        return cls.get_field_name(self.field) + " " + self.direction


def write():
    for obj in objects_to_write:
        obj._write()
    objects_to_write.clear()
    db.commit()