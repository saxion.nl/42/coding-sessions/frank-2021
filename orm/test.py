import orm

class User(orm.Table):
    id = orm.Field('INTEGER', primary_key=True)

    first_name = orm.Field('TEXT')
    last_name = orm.Field('TEXT')

    email = orm.Field('VARCHAR')
    password = orm.Field('VARCHAR')

    login_count = orm.Field('INTEGER', default=0)


class Comment(orm.Table):
    id = orm.Field('INTEGER', primary_key=True)
    user_id = orm.Field('INTEGER', foreign_key=(User,'id'))

    comment = orm.Field('TEXT')


class Like(orm.Table):
    user_id = orm.Field('INTEGER', foreign_key=(User,'id'), primary_key=True)
    comment_id = orm.Field('INTEGER', foreign_key=(Comment,'id'), primary_key=True)
    time = orm.Field('DATETIME')


User.create()
Comment.create()
Like.create()

print(User.get(123))
print(User.get(1))

print(Like.get(1, 2))

def register_user(first_name, last_name, email, password):
    User(first_name=first_name, last_name=last_name, email=email, password=password)
    orm.write()

def check_credentials(email, password):
    user = User.select_first("WHERE email=?", email)
    if user:
        if user.password == password:
            user.login_count += 1
            orm.write()
            print(f"User logged in for the {user.login_count}th time")
            return True
        print("Invalid password")
    else:
        print("No such user")
    return False

def find_naughty_students():
    users = User.select(((User.first_name=="Piet") & (User.login_count > 20)) | (User.last_name % "R%"), order_by=(User.login_count,User.id.desc()))
    for user in users:
        print(f"{user} --> {user!r}")

register_user("Piet", "Paulusma", "piet@weerbericht.nl", "oatmorn")
print(check_credentials("piet@weerbericht.nl", "oatmorn"))

print("Naught students:")
find_naughty_students()
