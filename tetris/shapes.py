SHAPE_STRINGS = [
"""
RR
RR
""",
"""
TTT
 T
""",
"""
L
L
LL
""",
"""
 J
 J
JJ
""",
"""
ZZ
 ZZ
""",
"""
 SS
SS
""",
"""
I
I
I
I
"""
]

SHAPE_COLORS = {
    "R": "orange",
    "T": "purple",
    "L": "yellow",
    "J": "blue",
    "Z": "red",
    "S": "green",
    "I": "cyan",
}