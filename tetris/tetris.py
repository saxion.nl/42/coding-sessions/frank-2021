import blessed
import shapes
import random
import time
from pygame import mixer

WIDTH = 10
HEIGHT = 22

term = blessed.Terminal()


def create_block():
    shape_str = random.choice(shapes.SHAPE_STRINGS)
    shape = get_shape_coordinates(shape_str)
    char = shape_str.lstrip()[0]
    return {
        "x": WIDTH//2 - get_shape_width(shape)//2,
        "y": 1,
        "shape": shape, # set[tuple]
        "color": shapes.SHAPE_COLORS[char] # string
    }

# Each block is a dictionary containing:
#   x: position from the left
#   y: position from the top
#   shape: set(tuple)
#   color: str


def get_left():
    return term.width // 2 - WIDTH // 2


def print_blocks(blocks):
    color = random.choice(list(shapes.SHAPE_COLORS.values()))
    print(getattr(term, f"{color}_on_black"), end='')
    print(term.clear, end='')

    print_borders()
    for block in blocks:
        print_block(block)
    print('', end='', flush=True)


def print_borders():
    left = get_left()
    right = left + WIDTH
    for y in range(HEIGHT):
        print(term.move_xy(left-2, y) + "||", end='')
        print(term.move_xy(right, y) + "||", end='')

    print(term.move_xy(left-2, HEIGHT) + '\=' + '=' * WIDTH + '=/')


def get_shape_coordinates(shape_str):
    result = set()
    shape_str = shape_str.strip("\n").split("\n")
    for y_offset, line in enumerate(shape_str):
        for x_offset, char in enumerate(line):
            if char != ' ':
                result.add((x_offset,y_offset,))
    return result


def get_block_coordinates(block):
    return set((x+block["x"],y+block["y"]) for x,y in block["shape"])


def print_block(block):
    # char = block["shape"].lstrip()[0]
    left = get_left()
    print(getattr(term, "on_"+block["color"]), end="")
    for x,y in get_block_coordinates(block):
        print(term.move_xy(left+x,y) + " ", end="")


def rotate_block(block):
    width = max((x for x,y in block["shape"]))
    block["shape"] = {(width-y,x) for x,y in block["shape"]}

def handle_key(blocks, key):
    current_block = blocks[-1]
    previous_x = current_block["x"]
    if key.name == "KEY_LEFT":
        current_block["x"] -= 1
    elif key.name == "KEY_RIGHT":
        current_block["x"] += 1
    elif key.name == "KEY_DOWN":
        current_block["y"] += 1
        while not collision_detected(blocks):
            current_block["y"] += 1
        current_block["y"] -= 1
    elif key.name == "KEY_UP":
        rotate_block(current_block)

    if collision_detected(blocks):
        current_block["x"] = previous_x


def collision_detected(blocks):
    current_block = blocks[-1]

    # Check collisions with the sides and bottoms of the play area.
    if current_block["y"] + get_shape_height(current_block["shape"]) > HEIGHT:
        return True
    if current_block["x"] < 0:
        return True
    if current_block["x"] + get_shape_width(current_block["shape"]) > WIDTH:
        return True

    # Check collisions with all of the other blocks.
    current_coords = get_block_coordinates(current_block)
    for other_block in blocks[0:-1]:
        other_coords = get_block_coordinates(other_block)
        if other_coords.intersection(current_coords):
            return True

    return False


def get_shape_width(shape):
    return max(x for x,y in shape) + 1


def get_shape_height(shape):
    return max(y for x,y in shape) + 1


def main():
    mixer.init()
    mixer.music.load('tetris.mp3')
    mixer.music.play(loops = -1)

    blocks = [create_block()]

    with term.fullscreen(), term.cbreak(), term.hidden_cursor():
        while True:
            # Wait for keys and process them for 500ms
            end_time = time.time() + 0.5
            while time.time() < end_time:
                print_blocks(blocks)
                key = term.inkey(timeout=end_time - time.time())
                handle_key(blocks, key)

            # Move the current block down
            blocks[-1]["y"] += 1
            if collision_detected(blocks):
                # The block reached the bottom, create a new one
                blocks[-1]["y"] -= 1
                blocks.append(create_block())
                if collision_detected(blocks):
                    break

        mixer.init()
        mixer.music.load('gameover.mp3')
        mixer.music.play()
        while mixer.music.get_busy():
            time.sleep(0.1)

    print("You lose!")

main()

