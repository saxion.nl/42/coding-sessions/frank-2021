# Adventure game

## Class diagram

```plantuml
class Player {}
class Enemy {
    +is_heat_resistent(): bool
}
class Item {
    -name: str
    -weight: number
    +get_weight(): number
    +get_name(): str
}

class Entity {
    -position: tuple
}

class Game {}
class Board {}

Board o--> "0..*" Entity

Entity <|-- Player
Entity <|-- Enemy
Entity <|-- Item

Player o--> "0..*" Item
Enemy o--> "0..*" Item

Game --> Board
Game --> Player

class Weapon {
    -damage: number
    -range: number
    +attack(enemy: Enemy)
}

Item <|-- Weapon
Item <|-- HealthPotion
```