class Game:

    MAX_LEVEL = 10

    def __init__(self):
        self.level = 0
        self.player = Player()

    def run(self):
        while level < self.MAX_LEVEL:
            self.board = Board(f"level{level}.csv", player)
            self.board.play()


class Board:
    def __init__(self, csvfile, player):
        self.squares = dict() # {(x,y) => Square}
        self.player = player
        # TODO: load the level into Squares
    
    def play(self):
        # TODO: game
        pass

class Item:
    def __init__(self, name, weight):
        self.name = name
        self.weight = weight
        self.item_health = 100

    def get_weight(self):
        return self.weight

    def get_name(self):
        return self.name
    
class Weapon(Item):
    def __init__(self, name, weight, damage, range):
        super().__init__(name, weight)
        self.damage = damage
        self.range = range

    def attack(self, enemy):
        enemy.do_damage(self.damage)
        