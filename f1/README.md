# Class diagram

```plantuml
class Car
class Part
class Team
class Driver

class Race {
    +lap_count: int
    +weather_condition: str
    
}

class Track {
    +length_in_m: int
    +location: str
    +elevation_in_m: int
    +height_differential_in_m: int
}

class Championship {
    +year: int
}
Championship --> "*" Race
Championship --> "*" Team

```