import blessed
import random
from abc import ABC, abstractmethod
import time


FPS = 60
LANE_COUNT = 5
MAX_DISTANCE = 100

CAR_LENGTH = 5
CAR_WIDTH = 0.6 # fraction of the lane width

LINE_LENGTH = 4
GAP_LENGTH = 6
SPEED = 15

BLINK_TIME = FPS*1.5

HORIZON_FRACTION = 0.0751398 # (C) indy

COLOR_SPEEDS = {
    'green': 1.2,
    'yellow': 0.9,
    'orange': 0.6,
    'red': 0.3,
}


class Car(ABC):
    def __init__(self, lane, distance, char):
        self.lane = lane
        self.distance = distance
        self.char = char

    def draw(self, term):
        y_bottom = dither_round(distance_to_y(self.distance, term))
        y_top = dither_round(distance_to_y(self.distance + CAR_LENGTH, term))

        print(getattr(term, self.color), end="")

        for y in range(y_top, min(y_bottom+1, term.height)):

            vx_margin = (1-CAR_WIDTH) / LANE_COUNT / 2
            vx_left = self.lane / LANE_COUNT + vx_margin
            vx_right = (self.lane+1) / LANE_COUNT - vx_margin

            x_left = dither_round(vx_to_x(vx_left, y, term))
            x_right = dither_round(vx_to_x(vx_right, y, term))

            print(term.move_xy(x_left, y) + self.char*max(1, x_right-x_left), end="")

    def collides_with(self, other):
        return self.lane == other.lane and self.distance+CAR_LENGTH >= other.distance and other.distance+CAR_LENGTH >= self.distance


class NPC(Car):
    def __init__(self):
        super().__init__(random.randint(0, LANE_COUNT-1), MAX_DISTANCE, "v")
        self.color = random.choice(list(COLOR_SPEEDS.keys()))

    @property
    def speed(self):
        return COLOR_SPEEDS[self.color]

    def move(self):
        self.distance -= self.speed

    def is_gone(self):
        return self.distance <= -CAR_LENGTH



class Player(Car):
    def __init__(self):
        super().__init__(LANE_COUNT//2, 10, "^")
        self.color = 'blue'
        self._blink_counter = None

    def move(self, delta):
        self.lane = min(LANE_COUNT-1, max(0, self.lane+delta) )

    def draw(self, term):
        if self._blink_counter != None:
            self._blink_counter += 1
            if self._blink_counter > BLINK_TIME:
                self._blink_counter = None

        if not self.is_blinking or (self._blink_counter & 8):
            super().draw(term)

    def blink(self):
        self._blink_counter = 0

    @property
    def is_blinking(self):
        return self._blink_counter != None



def dither_round(value):
    rounded_down = int(value)
    fraction = value - rounded_down
    return rounded_down + 1 if random.random() < fraction else rounded_down

def y_to_distance(y, term):
    return SPEED / (y / (term.height-1) + 0.2)

def distance_to_y(distance, term):
    if distance <= 0:
        return term.height-1
    else:
        return max(0, (SPEED/distance - 0.2) * (term.height-1))

def vx_to_x(vx, y, term):
    y_fraction = y / (term.height-1) * (1-HORIZON_FRACTION) + HORIZON_FRACTION
    interpolated = y_fraction*vx + (1-y_fraction)*0.5
    return (term.width-1) * interpolated




class Game:

    def __init__(self):
        self._line_offset = 0

    def play(self):
        player = Player()
        npcs = [NPC()]
        term = blessed.Terminal()
        lives = 5

        with term.fullscreen(), term.cbreak(), term.hidden_cursor():
            while lives > 0:
                print(term.home + term.clear + term.blue + f"LIVES: {lives}", end="")

                self.draw_lanes(term)

                if random.randint(0,FPS/2)==0:
                    npcs.append(NPC())

                for npc in npcs:
                    npc.move()
                    if npc.is_gone():
                        npcs.remove(npc)
                    else:
                        if not player.is_blinking and npc.collides_with(player):
                            player.blink()
                            lives -= 1
                        npc.draw(term)

                player.draw(term)

                val = term.inkey(timeout=0)
                if val:
                    if val.name == "KEY_LEFT":
                        player.move(-1)
                    elif val.name == "KEY_RIGHT":
                        player.move(+1)

                print("", end="", flush=True)
    
                time.sleep(1.0 / FPS)
                
        print("You died!")


    def draw_lanes(self, term):
        # Every redraw the dashed lines will need to get a bit closer
        self._line_offset -= 1.5
        if self._line_offset < 0:
            self._line_offset = LINE_LENGTH + GAP_LENGTH - 1

        print(term.white, end="")

        screen_width = term.width-1

        # The number of lines to draw is one more than the number of lanes,
        # because we need to draw side lines as well.
        for vx in [lane/LANE_COUNT for lane in range(LANE_COUNT+1)]:
            prev_x = screen_width // 2
            prev_vx = 0.5

            # Paint the line from top to bottom
            for y in range(0, term.height):
                # Do we draw the line at the y coordinate?
                if vx not in (0, 1,):
                    # vx==0 is the left (continuous) line and vx==1 is the right (continuous) line
                    # otherwise it depends on the line_offset.
                    real_life_distance = y_to_distance(y, term)
                    if dither_round(self._line_offset - real_life_distance + vx) % (LINE_LENGTH + GAP_LENGTH) < GAP_LENGTH:
                        continue

                # Calculate the screen x coordinate that goes with the virtual x coordinate (vx)
                x = dither_round(vx_to_x(vx, y, term))

                # Make sure that the dithering doesn't cause the lines to meander
                if vx < 0.5:
                    x = min(x, prev_x)
                else:
                    x = max(x, prev_x)

                # Print the actual line segment
                symbol = "/" if x<prev_x else ("\\" if x>prev_x else "|")
                print(term.move_xy(x, y) + symbol, end="")

                prev_x = x
                prev_vx = vx


            

if __name__ == "__main__":
    Game().play()
