import json
from collections import defaultdict

with open('boulders.json') as file:
    json_boulders = file.read()
    boulders = json.loads(json_boulders)

grade_counts = defaultdict(int) # {grade: count}


print()

# Show counts per grade
for boulder in boulders:
    grade = boulder["grade"]
    grade_counts[grade] += 1
while len(grade_counts) > 0:
    lowest_grade = None
    for grade, count in grade_counts.items():
        if lowest_grade == None or count < grade_counts[lowest_grade]:
            lowest_grade = grade
    print(f"{lowest_grade}: {grade_counts[lowest_grade]}")
    del grade_counts[lowest_grade]

# Show min and max x and y
xs = [float(boulder["position_x"]) for boulder in boulders]
ys = [float(boulder["position_y"]) for boulder in boulders]
print(min(xs),max(xs), min(ys),max(ys))


def grade_to_color(grade):
    # 2 ==> #00ff00 green
    # 8 ==> #ff0000 red
    red = round((grade - 2) / 6 * 255)
    green = 255 - red
    return f"#{red:02x}{green:02x}00"


def write_html(filename, body, title):
    output = f"""<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="style.css">
        <title>{title}</title>
    </head>
    <body>
        {body}
    </body>
    </html>"""

    with open(filename, "w") as file:
        file.write(output)


def write_map_html(name, boulders, category_names):
    boulders_html = ''
    for boulder in boulders:
        x = float(boulder["position_x"])
        y = float(boulder["position_y"])
        grade = float(boulder["grade"])
        
        boulders_html += f"""<a href="boulder-{boulder['id']}.html" target="sidebar" class="boulder" style="left: {x*100}%; top: {y*100}%; background-color: {grade_to_color(grade)};"></a>\n"""

    category_links = []
    for cname in category_names:
        category_links.append(f'<a href="{cname.lower()}.html">{cname}</a>')

    html = f"""
<main>
    <nav>
        {"".join(category_links)}
    </nav>
    <div class="room">{boulders_html}</div>
</main>
<iframe name="sidebar" src="about: blank;">
"""
    write_html(f"{name.lower()}.html", html, "Boulders")


def main():
    # All / easy / medium / hard / beer / kids proof

    categories = {
        "All": boulders,
        "Easy": [boulder for boulder in boulders if 3 <= float(boulder['grade']) < 5],
        "Medium": [boulder for boulder in boulders if 5 <= float(boulder['grade']) < 6.5],
        "Hard": [boulder for boulder in boulders if 6.5 <= float(boulder['grade'])],
        "Beer": [boulder for boulder in boulders if float(boulder['grade']) < 3],
    }

    for name, category_boulders in categories.items():
        write_map_html(name, category_boulders, categories.keys())


    for boulder in boulders:
        name = boulder['name'] if 'name' in boulder else "#"+str(boulder['id'])

        html = f"""
<h2>{name}</h2>
<table>
<tr>
    <th>Grade</th><td>{boulder['grade']}</td>
</tr><tr>
    <th>Kids proof</th><td>{'yes' if boulder['suitable_for_kids'] else 'no'}</td>
</tr><tr>
    <th># ascends</th><td>{boulder['nr_of_ascends']}</td>
</tr>
</table>
        """

        write_html(f"boulder-{boulder['id']}.html", html, f"Boulder {name}")


if __name__ == "__main__":
    main()
