import unittest
from n_queens import *

class Test(unittest.TestCase):
    def test_placement(self):
        self.assert_(check_placement(set(), (0,0)))

        # Two queens on one square
        self.assertFalse(check_placement({(1,1)}, (1,1)))

        # Horizontal
        self.assertFalse(check_placement({(1,1)}, (1,2)))
        # Vertical
        self.assertFalse(check_placement({(1,1)}, (7,1)))
        # Vertical and horizontal allowed
        self.assert_(check_placement({(1,1)}, (7,2)))
        self.assert_(check_placement({(4,2)}, (3,5)))

        # Diagonal
        self.assertFalse(check_placement({(1,1)}, (3,3)))
        self.assertFalse(check_placement({(3,6)}, (2,5)))
        self.assertFalse(check_placement({(0,6)}, (1,7)))


if __name__ == "__main__":
    unittest.main(Test())