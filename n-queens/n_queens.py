BOARD_SIZE = 42

def print_board(queens):
    for y in range(BOARD_SIZE):
        for x in range(BOARD_SIZE):
            print(" ♛" if (y,x,) in queens else " .", end="")
        print()

def check_placement(queens: set, pos: tuple):
    """Check if placing a queen on the specified coordinates is 
    possible without any queens attacking each other.

    Args:
        queens (set[tuple[int]]): The set of queen coordinates.
        pos (tuple[int]): The y and x coordinate.

    Returns:
        bool: True in case no attacks are possible. False otherwise.
    """

    pass

    # Check for horizontal queens
    for x in range(BOARD_SIZE):
        if (pos[0], x) in queens:
            return False

    # Check for vertical queens
    for y in range(BOARD_SIZE):
        if (y, pos[1]) in queens:
            return False

    # Check for diagonal queens
    for queen in queens:
        y_dist = abs(queen[0] - pos[0])
        x_dist = abs(queen[1] - pos[1])
        if y_dist == x_dist:
            return False

    return True

def solve(queens: set, queens_left):
    if queens_left <= 0:
        return True

    for y in range(BOARD_SIZE):
        for x in range(BOARD_SIZE):
            pos = (y,x,)
            if check_placement(queens, pos):
                queens.add(pos)
                if solve(queens, queens_left-1):
                    return True
                queens.remove(pos)

    return False                


for n in range(1, BOARD_SIZE+1):
    print(f"\n{n} queens!")
    queens = set()
    if solve(queens, n):
        print_board(queens)
    else:
        print("FAILED!")
        break

# queens.add((2,4))
# print_board(queens)