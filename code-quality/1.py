
def translate_leet(toTranslate):
    toTranslate = toTranslate.replace("elite", "leet")
    toTranslate = toTranslate.replace("hacker", "haxor")
    toTranslate = toTranslate.replace("a", "4")
    toTranslate = toTranslate.replace("e", "3")
    toTranslate = toTranslate.replace("i", "1")
    toTranslate = toTranslate.replace("o", "0")
    translated = toTranslate
    return translated

def translate_half(toTranslate):
    length = len(toTranslate)
    length = int(length / 2)
    toTranslate = toTranslate.split(toTranslate[length])[0]
    translated = toTranslate + "..."
    return translated

def translate_yoda(sentence, word_count=1):
    for _ in word_count:
        last_space_index = sentence.rfind(' ')
        first_words = sentence[:last_space_index]
        last_word = sentence[last_space_index+1:]
        sentence = last_word + ' ' + first_words


def ask_number(prompt):
    while True:
        try:
            return int(input(prompt))
        except ValueError:
            print("Invalid input, please try again")


print("*** Welcome to the babelfish translator ***")
inp = input("Enter the text you want to translate (type 'exit' to quit the application): ").lower()

if inp == 'exit':
    exit()

#bypasses the need for \n which could otherwise be done in a normal string as suck "...translate the text:\n1. ..."
print("""
How do you want to translate the text:
1. Translate to leet speak
2. Translate to half speak
3. Translate to leet Yoda speak
4. Translate to leet Yodadada speak""")

option = input("Please enter your selection: ").lower().strip()

while True:
    if option == 'exit':
        exit()
    elif option == "1":   
        result = translate_leet(inp)
    elif option == "2":
        result = translate_half(inp)
    elif option == "3":
        result = translate_leet(translate_yoda(inp))
    elif option == "4":
        result = translate_yoda(inp, ask_number("Please enter how many words you want to move: "))
    else:
        option = input("Please select a valid option (1, 2, 3, 4): ")
        continue

    print("The text has been translated to:", result)
    break

exit()