
print("*** Welcome to the babelfish translator ***")
translatableText = input("Enter the text you want to translate (type 'exit' to quit the application): ")
if  translatableText.lower() == "exit":
    print("Ok, bye now!")
    exit()  

# Function for translatable Leet words.
def textToLeetWords():
    global translatableText
    eliteCount = translatableText.count("elite")
    hackerCount = translatableText.count("hacker")
    print(f"Number of ocurrences: elite = {eliteCount}, hacker = {hackerCount}")
    translatableText = translatableText.replace("elite", "leet")
    translatableText = translatableText.replace("hacker", "haxor")

# Function for translatable Leet numbers.
def textToLeetNumbers():
    global translatableText

    # Replace the various letters with the corresponding numbers
    translatableText = translatableText.replace("a", "4")
    translatableText = translatableText.replace("e", "3")
    translatableText = translatableText.replace("i", "1")
    translatableText = translatableText.replace("o", "0")

# Function for splitting the text to half.
def textToHalf():
    global translatableText

    # show the total length of characters inside the string.
    textLength = len(translatableText)
    print(f"the sentence is '{textLength}' characters long.")

    # Calculate and display the center index of the string.
    textCenterIndex = round(len(translatableText) / 2)
    print(f"The center is located  at index '{textCenterIndex}'")

    # Split the string and display the first half of the string.
    print(translatableText[:len(translatableText) // 2])

# Function for translating text to Yoda speak
def textToYoda(counter = 1):
    global translatableText

    # Move the last word tot the beginning of the sentence and display it.
    translatableText = translatableText[translatableText.rfind(" ") + 1:] + " " + translatableText[:translatableText.rfind(" ")]
    print(translatableText)

    # More Yoda speak
    for x in range(counter - 1):
        translatableText = translatableText[translatableText.rfind(" ") + 1:] + " " + translatableText[:translatableText.rfind(" ")]

    print(translatableText)


# Options menu.
print("""
    How do you want to translate the text:
    1. Translate to leet speak
    2. Translate to half a sentence
    3. Translate to Yoda speak
""")

selectedOption = input("Please enter your selection: ")

# Main while-loop for the options menu
while selectedOption:

    # Leet speak
    if selectedOption == "1":
        textToLeetWords()
        textToLeetNumbers()
        print(translatableText)
        break

    # Half a sentence
    elif selectedOption == "2":
        textToHalf()
        break

    # Yoda speak
    elif selectedOption == "3":
        textToLeetWords()
        textToLeetNumbers()
        textToYoda(3)
        break

    else:
        selectedOption = input("Please select a valid option(1, 2 or 3): _")