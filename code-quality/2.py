
# the option definition
# option 1 translate_leet

def translate_leet(user_text):
    """translate_leet
    replace words elite and hacker also replace the letters a,e,i and o into numbers"""
    user_text = user_text.replace("elite", "leet").replace("hacker", "haxor") #replace elite and hacker
    user_text = user_text.replace("a", "4").replace("e", "3").replace("i", "1").replace("o", "0") # replace a,e,i,o
    return user_text

#option 2 half a sentence

def option_2(user_text):
    """ Translate to half speak
    Prints half of the user_text followed by ..."""
    half_user_text = user_text[:len(user_text) // 2] # divide the sentence
    print(f"The text has been translated to: {half_user_text}...")

#option 3 yoda speak

def option_3(user_text):
    """yoda speak
    takes the last word and puts it first"""
    last_space_index = user_text.rfind(" ") # where is the last space
    end_word = (user_text[last_space_index:]) # last space until the end 
    start_text = (user_text[:last_space_index]) 
    print (f"The text has been translated to:{end_word} {start_text}") # result

# option 3 yodo speak loop

def option_4(user_text, words_input):
    """yodadada speak
    takes the last word an puts it first x amount of time"""
    while not words_input == 0: #start loop stop at 0
        end = user_text.rfind(" ") 
        end_word = user_text[end:]
        end_text = user_text[:end]
        user_text = f"{end_word} {end_text}" # 1 word done
        words_input = words_input - 1   # how many loops
    print (f"The text has been translated to:{end_word}{end_text}") # result
 
#welcome

print("*** Welcome to the babelfish translator ***")

#start loop

while True:

    #ask user text
    user_text = input("""Enter the text you want to translate (type 'exit' to quit the application):""")
    if user_text.lower() == 'exit':
        exit()

    # the menu
    print("""How do you want to translate the text:
    1. Translate to leet speak
    2. Translate to half speak
    3. Translate to leet Yoda speak
    4. Translate to leet Yodadada speak""")

    #loop to check for valid input
    while True: 
        try:
            user_input = int(input("Please enter your selection: ")) 
        except ValueError:
            print("please input a valid option ( 1,2,3,4)")
            continue
        if user_input < 1:
            print("please input a valid option ( 1,2,3,4)")  
            continue
        elif user_input > 4:
            print("please input a valid option ( 1,2,3,4)")
            continue
        else:
            break

# the option

    if user_input == 1:
        translate_leet(user_text)

    elif user_input == 2:
        option_2(user_text)

    elif user_input == 3:
        option_3(user_text)

#loop to check for valid input

    elif user_input == 4:
        while True:    
            try:
                words_input = int(input("Please enter how many words you want to move:"))
            except ValueError:
                print("please input a positive integer number")
                continue
            if words_input < 0:
                print("please input a positive integer number")
                continue
            else:
                break
        option_4(user_text, words_input)



