
def regex_search(text, regex):
    blocks = parse_regex(regex)
    for start_pos in range(len(text)):
        match = regex_test(text, start_pos, blocks)
        if match:
            print("match!", match)

# def regex_test(text, start_pos, blocks):
#     pos = start_pos
#     for block in blocks:
#         if text[pos] not in block["chars"]:
#             return None
#         pos += 1
#     return text[start_pos:pos]

def regex_test(text, start_pos, blocks):
    if not blocks:
        return ""
    
    block = blocks[0]

    # Count how many characters (starting from start_pos) match with block["chars"]. Up to block["max"] times.
    pos = start_pos
    while pos < len(text) and text[pos] in block["chars"]  and (block["max"] is None or pos - start_pos < block["max"]):
        pos += 1

    for end_pos in range(pos, start_pos+block["min"]-1, -1):
        result = regex_test(text, end_pos, blocks[1:])
        if result != None:
            return text[start_pos:end_pos] + result

    return None


def parse_regex(regex):
    """
    donkey --> [{"chars": "d", "min": 1, "max": 1}, {"chars": "o", "min": 1, "max": 1}, {"chars": "n", "min": 1, "max": 1}, ...]
    [dD]o+nkey --> [{"chars": "dD", "min": 1, "max": 1}, {"chars": "o", "min": 1, "max": None}, {"chars": "n", "min": 1, "max": 1}, ...]
    """

    results = []

    pos = 0
    while pos < len(regex):
        char = regex[pos]
        pos += 1

        if char == "*":
            results[-1]["min"] = 0
            results[-1]["max"] = None
        elif char == "?":
            results[-1]["min"] = 0
            results[-1]["max"] = 1
        elif char == "+":
            results[-1]["min"] = 1
            results[-1]["max"] = None
        elif char == "[":
            letters = ""
            while regex[pos] != "]":
                letters += regex[pos]
                pos += 1
            pos += 1
            results.append({"chars": letters, "min": 1, "max": 1})
        else:
            results.append({"chars": char, "min": 1, "max": 1})

    return results


with open("shrek.txt") as file:
    shrek = file.read()

print(parse_regex("[Dd]+o*nkey?"))
print(regex_search(shrek, "[Dd]o+nkey[!?]*"))
# regex_search("Dooooooonkey", "[Dd]o+nkey")


