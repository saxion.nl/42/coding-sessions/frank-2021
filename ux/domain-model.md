```plantuml

entity User {
    *name
    *email
    *password
    *is_admin?
}

entity Order {
    *date
    *deadline_time
    *delivery_time
    *delivery_cost
    *ready?
}

entity Item {
    *product_type
    *product_options
    *price
    *paid?
}

Order ||--o{ Item
Order "fetcher" }o--|| User
User ||--o{ Item

```
